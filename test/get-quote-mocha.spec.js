var browser = require('./core/browser.js'),
    co = require('co'),
    chai = require('chai'),
    moment = require('moment'),
    CensusPage = require('./pages/census-page.js');

describe('get quote', function () {
    this.timeout(99999999);

    it('should successfully get quote', function *(done) {
        yield browser.init();
        yield browser.setViewportSize({width: 1360, height: 768}, true);

        yield browser.url(CensusPage.url);

        var census = CensusPage.census;
        yield browser.click(census.gender.MALE);

        yield browser.click(census.dob.month);
        yield browser.pause(100);
        yield browser.setValue(census.dob.month, '1');

        yield browser.click(census.dob.day);
        yield browser.pause(100);
        yield browser.setValue(census.dob.day, '1');

        yield browser.click(census.dob.year);
        yield browser.setValue(census.dob.year, '1986');

        yield browser.setValue(census.zipCode, '77001');

        yield browser.click(CensusPage.CONTINUE);
        yield browser.saveScreenshot('./screenshots/2-results.png');
        yield browser.click('label[for="census_qualifyingLifeEventloss_of_coverage"]');
        yield browser.execute(function () {
            $("#census_dateOfEvent").val('05/01/2016').trigger("input").trigger("changeDate");
        });
        yield browser.click('#sep-continue-btn');
        yield browser.saveScreenshot('./screenshots/quote.png');
        yield browser.click("//a[@name='Apply'][1]");
        yield browser.saveScreenshot('./screenshots/cart.png');

        done();


    });
});