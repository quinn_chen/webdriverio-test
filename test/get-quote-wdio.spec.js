var chai = require('chai'),
    CensusPage = require('./pages/census-page.js');

describe('quote flow', function () {
    it('should successfully get quote', function () {
        this.timeout(999999999);

        browser.setViewportSize({width: 1360, height: 768}, true);

        browser.url(CensusPage.url);

        var census = CensusPage.census;
        browser.click(census.gender.MALE);

        browser.click(census.dob.month);
        browser.pause(100);
        browser.setValue(census.dob.month, '1');

        browser.click(census.dob.day);
        browser.pause(100);
        browser.setValue(census.dob.day, '1');

        browser.click(census.dob.year);
        browser.pause(100);
        browser.setValue(census.dob.year, '1986');

        browser.setValue(census.zipCode, '77001');

        browser.click(CensusPage.CONTINUE);
        browser.saveScreenshot('./screenshots/2-results.png');
        browser.click('label[for="census_qualifyingLifeEventhad_a_baby"]');
        
        browser.execute(function () {
            $('#census_dateOfEvent').val('05/01/2016').trigger("input").trigger("changeDate");
        });

        browser.saveScreenshot('./screenshots/3-results.png');
        browser.click('#sep-continue-btn');
        browser.saveScreenshot('./screenshots/quote.png');
        browser.debug();


    });
});