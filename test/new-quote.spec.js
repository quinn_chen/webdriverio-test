/**
 * Created by supingx on 5/31/2016.
 */
var browser = require('./core/browser.js'),
    co = require('co'),
    chai = require('chai'),
    moment = require('moment'),
    CensusPage = require('./pages/new-census.js');

describe('get quote', function () {
    it('should successfully get quote', function (done) {
        this.timeout(99999999);

        co(function *() {
            yield browser.init();
            yield browser.setViewportSize({width: 1121, height: 768}, true);

            yield browser.url(CensusPage.url); 
            var census = CensusPage.census;
            yield browser.waitForVisible(census.primary.dob,50000);
            yield browser.setValue(census.primary.dob, '01011977');
            yield browser.click(census.primary.married.MARRIED);
            yield browser.click(census.primary.children.PLUS)
            yield browser.saveScreenshot('./screenshots/primary.png');
            yield browser.click(CensusPage.next);

            yield browser.waitForVisible(census.spouse.dob, 50000);
            yield browser.setValue(census.spouse.dob, '01011978');
            yield browser.saveScreenshot('./screenshots/spouse.png');
            yield browser.click(CensusPage.next);

            yield browser.waitForVisible(census.children.dob, 50000);
            yield browser.setValue(census.children.dob, '01012006');
            yield browser.saveScreenshot('./screenshots/child.png');
            yield browser.click(CensusPage.next);

            yield browser.waitForVisible(census.qle.other, 50000);
            yield browser.click(census.qle.hadBaby);
            yield browser.waitForVisible(census.qle.eventDate,10000);
            yield browser.execute(function () {
                $("#census_dateOfEvent").val('05/01/2016').trigger("change");
                // $("span.pulse-btn.next").removeClass("disabled").addClass("pulsate");
            });
            yield browser.saveScreenshot('./screenshots/qle.png');
            yield browser.click(CensusPage.next);
            yield browser.waitForVisible(census.subsidy.income, 50000);
            yield browser.saveScreenshot('./screenshots/subsidy.png');

            done();

        }).catch(function (err) {
            done(err);
        })

    });
});