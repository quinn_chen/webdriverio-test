/**
 * Created by supingx on 5/31/2016.
 */
var browser = require('./core/browser.js'),
    chai = require('chai'),
    _ = require('lodash');

expect = chai.expect;


describe('get quote', function () {
    this.timeout(1000000);

    var fastQuoteUrls = [];
    readFileLines('./test/data/qleRedirect.text').forEach((url)=> {
        if (url) fastQuoteUrls.push(url);
    });

    _.each(fastQuoteUrls, function (url) {
        it(`should successfully get to ST quote for URL:${url}`, function *(done) {
            yield browser.init();
            yield browser.setViewportSize({width: 1121, height: 768}, true);
            yield browser.url(url);
            yield browser.waitForVisible('.overlay-header-primary', 50000);

            var title = yield browser.getTitle();
            var currentUrl = yield browser.getUrl();
            expect(title).to.be.equal('Health Insurance Quote Page - Short Term Insurance');
            expect(currentUrl).to.be.equal('https://www.cm.ehealthinsurance.com/ehi/st/best-sellers?fromPage=sep-screen-no-plan&sepIneligible=true');
            yield browser.session('delete');
            done();
        });

    });


});
/*  it('should successfully get to IFP quote', function (done) {
 this.timeout(50000);

 co(function *() {
 yield browser.url('https://www.cm.ehealthinsurance.com/?allid=Nor29268&zip=90001&type=IFP&gd1=Self(Male)&bdate1=1/1/1971&qle=COVERAGE&doe=5/23/2016');
 yield browser.waitForVisible('.fastquote-overlay', 50000);
 yield browser.getTitle().then(function (title) {
 expect(title).to.be.equal('Health Insurance Quote Page - Individual & Family')
 });


 done();

 }).catch(function (err) {
 done(err);
 })

 });*/



