/**
 * Created by supingx on 5/5/2016.
 */

var browser = require('./core/browser.js');
var co = require('co');

describe('qp.ehealthinsurance.com', function () {
    it('should successfully search book', function (done) {
        this.timeout(99999999);
        co(function*() {
            yield* browser.init();
            yield* browser.setViewportSize({width: 1360, height: 768}, true);
            yield* browser.url('https://www.qp.ehealthinsurance.com');
            yield* browser.saveScreenshot('./screenshots/home.png');
            yield* browser.pause(500);
            yield* browser.click("//a[@title='Health Insurance']");
            yield* browser.setValue('[for=census_primary_genderMALE]','true');

            yield* browser.saveScreenshot('./screenshots/census.png');
            done();

        }).catch(function (err) {
            done(err);
        });


    });
});
/*
describe('qa.ehealthinsurance.com', function () {
    it('should successfully get quote', function () {
        browser.setViewportSize({width: 1360, height: 768}, true);
        browser.url('https://www.qp.ehealthinsurance.com/');
        browser.saveScreenshot('./screenshots/home.png');
        browser.pause(500);

        browser.click('[title="Health Insurance"]').then(function(done){
            setTimeout(3000);
            browser.setValue('[for=census_primary_genderMALE]','true');
            browser.setValue('#census_primary_month','1');
            browser.setValue('#census_primary_day','1');
            browser.setValue('#census_primary_year','1977');
            browser.setValue('#census_zip','77001');
            browser.click('#continue-btn');
            browser.saveScreenshot('./screenshots/census.png')
            done();


        })



    });
});
