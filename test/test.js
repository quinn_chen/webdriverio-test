/**
 * Created by supingx on 5/9/2016.
 */
var browser = require('./core/browser.js'),
    co = require('co'),
    chai = require('chai'),
    moment = require('moment'),
    CensusPage = require('./pages/census-page.js');


describe('get quote', function () {
    it('should successfully get quote', function (done) {
        this.timeout(99999999);1111

        co(function *() {
            yield browser.init();
            yield browser.setViewportSize({width: 1360, height: 768}, true);

            yield browser.url(CensusPage.sepURL);


            //sep event
            yield browser.click('label[for="census_qualifyingLifeEventloss_of_coverage"]');
            yield browser.getValue('#census_dateOfEvent');
            yield browser.saveScreenshot('./screenshots/sep.png');
            yield browser.click(CensusPage.CONTINUE);
            yield browser.saveScreenshot('./screenshots/quote.png');
            done();

        }).catch(function (err) {
            done(err);
        })

    });
});