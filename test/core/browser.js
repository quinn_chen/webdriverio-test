var co = require('co'),
    fs = require('fs');

if (!global.isMochaWrappedWithCo) {
    var _it = it;
    global.it = (testName, genCallback) => {
        _it(testName, (done)=> {
            co(function *() {
                yield *genCallback(done);
            }).catch(function (err) {
                done(err);
            });
        });
    };

    global.isMochaWrappedWithCo = true;
}

if (!global.readFileLines) {
    global.readFileLines = (filePath)=> {
        return fs.readFileSync(filePath).toString().replace('\r', '').split('\n');
    }
}

if (global.browser) {
    module.exports = global.browser;
    return;
}


global.browser = {};

var options = {
    desiredCapabilities: {
        browserName: 'firefox',
        applicationCacheEnabled: true,
        pageLoadStrategy: 'eager'
    }
};
var webdriverio = require('webdriverio').remote(options);


module.exports = global.browser = webdriverio;

