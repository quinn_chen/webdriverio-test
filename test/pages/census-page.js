/**
 * Created by quinnc on 5/6/2016.
 */
var CensusPage = {
    url: 'https://www.cm.ehealthinsurance.com/individual-family-health-insurance',
    sepURL: 'https://www.cm.ehealthinsurance.com/individual-family-health-insurance#censusForm/sep',

    census: {
        gender: {
            MALE: 'label[for="census_primary_genderMALE"]',
            FEMALE: 'label[for="census_primary_genderFEMALE"]'
        },
        dob: {
            month: '#census_primary_month',
            day: '#census_primary_day',
            year: '#census_primary_year'
        },
        zipCode: '#census_zip'
    },

    CONTINUE: 'input[value="Continue"]'
};

module.exports = CensusPage;