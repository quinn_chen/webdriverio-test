/**
 * Created by supingx on 5/31/2016.
 */

var CensusPage = {
    url: 'https://www.sjdevifp08.ehealthinsurance.com/ifp-client/new-quote.html',

    census: {
        primary: {
            gender: {
                MALE: 'label[for="census_primary_genderMALE"]',
                FEMALE: 'label[for="census_primary_genderFEMALE"]'
            },
            dob: '#census_primary_dob',
            smoker: '#census_primary_tobacco',
            married: {
                SINGLE: 'label[for="census_primary_single"]',
                MARRIED: 'label[for="census_primary_married"]'
            },
            children: {
                REDUCE: 'span[data-key="reduce"]',
                PLUS: 'span[data-key="plus"]'
            }

        },
        spouse: {
            dob: '#census_spouse_dob',
            smoker: '#census_spouse_tobacco',
            remove: '.remove-btn spouse-remove'
        },
        children: {
            dob: '#child_1_dob',
            smoker: '#census_child1_tobacco',
            remove: '.remove-btn child-remove'
        },
        qle: {
            coverage: '#census_qualifyingLifeEventloss_of_coverage',
            marriage: '#census_qualifyingLifeEventgot_married_or_divorced',
            move: '#census_qualifyingLifeEventmoved_to_a_different_state',
            hadBaby: '#census_qualifyingLifeEventhad_a_baby',
            other: '#census_qualifyingLifeEventother',
            disqualify: '#census_qualifyingLifeEventi_dont_qualify',
            eventDate: '#census_dateOfEvent'
        },
        subsidy: {
            income: '#se_form_household_income',
            familySize: '#se_form_family_size'
        }
    },
    next: '.inset-arrow-right'
};

module.exports = CensusPage;